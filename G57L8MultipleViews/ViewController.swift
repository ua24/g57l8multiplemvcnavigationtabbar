//
//  ViewController.swift
//  G57L8MultipleViews
//
//  Created by Ivan Vasilevich on 10/19/17.
//  Copyright © 2017 Smoosh Labs. All rights reserved.
//

import UIKit

var str = ""

class ViewController: UIViewController {
	
	@IBOutlet var myLabel: UILabel!
	@IBOutlet var myTextField: UITextField!
	
	override func awakeFromNib() {
		super.awakeFromNib()
		checkLabel()
		print(#function)
	}

	override func viewDidLoad() {
		super.viewDidLoad()
		// Do any additional setup after loading the view, typically from a nib.
		checkLabel()
		print(#function)
	}
	
	override func viewWillAppear(_ animated: Bool) {
		super.viewWillAppear(animated)
//		view.backgroundColor = .blue
		print(#function)
		
	}
	
	override func viewWillLayoutSubviews() {
		super.viewWillLayoutSubviews()
		
		print(#function)
	}
	
	override func viewDidLayoutSubviews() {
		super.viewDidLayoutSubviews()
		
		print(#function)
	}
	
	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)
		
		print(#function)
	}
	
	override func viewWillDisappear(_ animated: Bool) {
		super.viewWillDisappear(animated)

		print(#function)
	}
	
	override func viewDidDisappear(_ animated: Bool) {
		super.viewDidDisappear(animated)
		
		print(#function)
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
//		print(#function)
	}
	
	override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
		str = myTextField.text ?? ""
	}

	func checkLabel() {
		if myLabel == nil {
			print("myLabel == nil")
		}
		else {
			print("myLabel != nil")
		}
	}
	
	override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
		performSegue(withIdentifier: "showBlue", sender: nil)
	}

}

